Ep2 Orientação a Objetos - 1/2017
Aluno : Nícalo Ribeiro
Matrícula : 16/0016169
Professor : Renato Coral

--------------------------------------------------

Space Shooter
Incorpore um piloto de uma espaçonave e derrote inimigos em uma batalha intergalática.
Sobreviva o máximo que conseguir.
A dificuldade do jogo varia de acordo com o número de inimgios eliminados: quanto mais inimigos eliminados, mais inimigos irão dar spawn.

Movimente-se com as setas do teclado e atire com a barra de espaço.
Abater inimigos lhe concede pontos. Será que consegue bater recordes? 


--------------------------------------------------

ATENÇÃO: Tive problemas em dar git em um primeiro repositório, então foi necessário a criação desse, mas aqui está o link do primeiro repositório com os primeiros commits : https://gitlab.com/nicaloribeiro/EP2/


Crétdios pelas sprites : Kenney Vleugels (www.kenney.nl)



