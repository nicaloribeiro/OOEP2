import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class Inimigos extends GameObject implements EntityB{
	
//	private Texturas tex;
	Random r = new Random();
	private Game game;
	private Controles c;
	private int velocidade = r.nextInt(4) + 1;
	Animacoes anim;
	
	public Inimigos(double x, double y, Texturas tex, Controles c, Game game){
		super(x, y);
		//this.tex = tex;	
		this.c = c;
		this.game = game;
		anim = new Animacoes(1, tex.inimigo[0], tex.inimigo[1],tex.inimigo[2]);
	}
	
	public void tick(){
		y += velocidade;
		
		if(y > Game.HEIGHT * Game.SCALE){ 
			velocidade = r.nextInt(4) + 1;
			x = r.nextInt(640);
			y = -10;
		}
		
		for(int i = 0; i <game.ea.size(); i++)
		{
			EntityA tempEnt = game.ea.get(i);
			if(Physics.Collision(this, tempEnt))
			{
				c.removeEntity(tempEnt);
				c.removeEntity(this);
				game.setInimigo_eliminado(game.getInimigo_eliminado() + 1);
				Game.setScore(Game.getScore() + (75));
				
			}
			
			
		}
		
		
		anim.runAnimation();
			
		
	}
	
	public void render(Graphics g){
		anim.drawAnimation(g, x, y, 0);
	}
	public Rectangle getBounds(){
		return new Rectangle((int)x, (int)y, 32, 32);
	}
	
	public double getY(){
		return y;
	}
	public void setY(double y){
		this.y = y;
	}
	public double getX(){
		return x;
	}
	public void setX(double x){
		this.x = x;
	}
}
