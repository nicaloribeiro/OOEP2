import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Game extends Canvas implements Runnable  {
	

	private static final long serialVersionUID = 1L;
	public static final int WIDHT = 320;
	public static final int HEIGHT = 260;
	public static final int SCALE = 2;
	public final String TITLE = "EP2 - Space Game";
	
	private boolean running = false;
	private Thread thread;
	
	private BufferedImage image = new BufferedImage(WIDHT,HEIGHT,BufferedImage.TYPE_INT_RGB);
	private BufferedImage spriteSheet = null;
	private BufferedImage background = null;
	private boolean atirando = false;
	private Player p;
	private Controles c;
	private Texturas tex;
	private Menu menu;
	public LinkedList<EntityA> ea;
	public LinkedList<EntityB> eb;
	public static int VIDA = 100;
	private int inimigo_gerado = 15;
	private int inimigo_eliminado = 0;
	private static int score = 0;
	private int HighScore = 0;
	File f = new File("HighScore.txt");
	public static enum STATE{
		MENU,
		GAME
	};
	public static STATE State = STATE.MENU;
	
	
	public void init(){
		requestFocus();
		CarregaImagem loader = new CarregaImagem();
		try{
			spriteSheet = loader.carregaImagem("/sprite2.png");
			background = loader.carregaImagem("/background.png");
		}
		catch (IOException e){
			e.printStackTrace();
			}
	
	addKeyListener(new TeclaInput(this));
	
	tex = new Texturas(this);	
	c = new Controles(tex, this);
	p = new Player(320,480, tex, this, c);
	menu = new Menu();
	
	ea = c.getEntityA();
	eb = c.getEntityB();
	
	c.criaInimigo(inimigo_gerado);
	
	this.addMouseListener(new MouseInput());
	}
	private synchronized void start(){
		if(running)
			return;
		running = true;
		thread = new Thread(this);
		thread.start();
	}
	private synchronized void stop(){
		if(!running)
			return;
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.exit(1);
		
		
	}
			
	
	public void run(){
		init();
		long lTime = System.nanoTime();
		final double nFps = 60.0;
		double ns = 1000000000 / nFps;
		double beta = 0;
		int updates = 0;
		int fps = 0;
		long temp = System.currentTimeMillis();
		
				
		while(running){
			
			long nTime = System.nanoTime();
			beta += (nTime - lTime)/ ns;
			lTime = nTime;
			if (beta >= 1) {
				tick();
				updates++;
				beta --;
			}
			render();
			fps++;
			if(System.currentTimeMillis() - temp > 1000) {
				temp +=1000;
				System.out.println(updates + "Ticks, Fps" + fps);
				updates = 0;
				fps = 0;
			}
			
		}
		stop();
		
	}
	
	private void tick(){
		if(State == STATE.GAME){
		p.tick();
		c.tick();
		}
		if(inimigo_eliminado >= inimigo_gerado - 5){
			inimigo_gerado += 3;
			 inimigo_eliminado = 0;
			 c.criaInimigo(inimigo_gerado);
		}
		try {
            Scanner fileScan = new Scanner(f);
            while (fileScan.hasNextInt()) {
                String nextLine = fileScan.nextLine();
                Scanner lineScan = new Scanner(nextLine);
                HighScore = lineScan.nextInt();
            }
        } catch (FileNotFoundException e) {
        }
	 	
      	 try {
                if (score > HighScore) {
                    String scoreString = Integer.toString(score);
                    PrintWriter pw = new PrintWriter(new FileOutputStream(f, false));
                    pw.write(scoreString);
                    pw.close();
                }
	 		} catch (FileNotFoundException e) {
     }
	 
	
	if(VIDA < 0){
		JOptionPane.showMessageDialog(null,"Obrigado por jogar!Sua pontua��o foi de " + score );
		System.exit(1); 
			
		
	
						
		}
	}

	 private void render(){
		 BufferStrategy bs = this.getBufferStrategy();
		 if(bs == null){
			 createBufferStrategy(3);
			 return;
		 }
		 Graphics g = bs.getDrawGraphics();
		 
		 g.drawImage(image,0, 0, getWidth(),getHeight(), this);
		 g.drawImage(background, 0, 0, null);
		 if(State == STATE.GAME){
		 p.render(g);
		 c.render(g);
		 g.setColor(Color.red);
		 g.fillRect(5, 5, 100, 10);
		 
		 g.setColor(Color.green);
		 g.fillRect(5, 5, VIDA, 10);
		 
		 g.setColor(Color.white);
		 g.drawRect(5, 5, 100, 10);
		 
		 g.setColor(Color.WHITE);
		 g.drawString("SCORE: " + getScore(), 5, 40);
		 
		 g.setColor(Color.white);
		 g.drawString("HighScore: " + HighScore , 5, 60);
		 
	}else if( State == STATE.MENU){
		menu.render(g);
		
	}
		 g.dispose();
		 bs.show();
				
	 }
	 
	 
          
	            
	          
	 
	 
	 public void keyPressed(KeyEvent e){
		 int key = e.getKeyCode();
		 
		 if( State == STATE.GAME){
		 if(key == KeyEvent.VK_RIGHT){
			 p.setVelX(5);			 
		 } else if (key == KeyEvent.VK_LEFT){
			 p.setVelX(-5);	 
		 } else if (key == KeyEvent.VK_DOWN){
			 p.setVelY(5);				 
		 } else if (key == KeyEvent.VK_UP){
			 p.setVelY(-5);				 
		 }else if(key == KeyEvent.VK_SPACE &&  !atirando){
			 atirando = true;
			 c.addEntity(new Bullet(p.getX(), p.getY(), tex, this));
		 }
	}
			
		}
		public void keyReleased(KeyEvent e){
			 int key = e.getKeyCode();
			 
			 if(key == KeyEvent.VK_RIGHT){
				 p.setVelX(0);			 
			 } else if (key == KeyEvent.VK_LEFT){
				 p.setVelX(0);	 
			 } else if (key == KeyEvent.VK_DOWN){
				 p.setVelY(0);				 
			 } else if (key == KeyEvent.VK_UP){
				 p.setVelY(0);				 
			 }else if( key == KeyEvent.VK_SPACE){
				 atirando = false;
			 }
			
		}
	
	public static void main(String args[]){
		Game game = new Game();
		
		game.setPreferredSize(new Dimension(WIDHT * SCALE, HEIGHT * SCALE));
		game.setMaximumSize(new Dimension(WIDHT * SCALE, HEIGHT * SCALE));
		game.setMinimumSize(new Dimension(WIDHT * SCALE, HEIGHT * SCALE));
		
		JFrame frame = new JFrame(game.TITLE);
		frame.add(game);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		
		game.start();	
 
		
	}
	
	
	public BufferedImage getSpriteSheet(){
		return spriteSheet;
	}
	public int getInimigo_gerado() {
		return inimigo_gerado;
	}
	public void setInimigo_gerado(int inimigo_gerado) {
		this.inimigo_gerado = inimigo_gerado;
	}
	public int getInimigo_eliminado() {
		return inimigo_eliminado;
	}
	public void setInimigo_eliminado(int inimigo_eliminado) {
		this.inimigo_eliminado = inimigo_eliminado;
	}
	public static int getScore() {
		return score;
	}
	public static void setScore(int score) {
		Game.score = score;
	}

 }
