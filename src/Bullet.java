import java.awt.Graphics;
import java.awt.Rectangle;

public class Bullet extends GameObject implements EntityA{
	
	// private Texturas tex;
	// private Game game;
	
	Animacoes anim;
	
	public Bullet( double x, double y, Texturas tex, Game game){
		super(x, y);
		//this.tex = tex;
		// this.game = game;
		
		anim = new Animacoes(5, tex.tiro[0], tex.tiro[0], tex.tiro[0]);
		
		
	}
	public void tick(){
		y -= 8;
		
	/*	if(Physics.Collision(this, game.eb))
		{
			System.out.println("COLISAO DETECTADA");
			
		} */
		anim.runAnimation();
	}
	
	public Rectangle getBounds(){
		return new Rectangle((int)x, (int)y, 32, 32);
	}
	
	public void render(Graphics g){
		anim.drawAnimation(g, x, y, 0);
	}
	public double getY(){
		return y;
	}
	
	public double getX() {
		return x;
	}

}
