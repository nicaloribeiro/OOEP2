import java.awt.Graphics;
import java.awt.Rectangle;

public class Player  extends GameObject implements EntityA{
	
	private double velX = 0;
	private double velY = 0;
	Game game;
	Controles controles;
	//private Texturas tex;
	Animacoes anim;
	
	public Player(double x, double y, Texturas tex, Game game, Controles controles){
		super(x,y);
		this.game = game;
		this.controles = controles;
		//this.tex = tex;
		
		anim = new Animacoes(2,tex.player[0], tex.player[1], tex.player[2]);
		
	}
	
	public void tick(){
		x+=velX;
		y+=velY;
		
		if(x <= 0)
			x=0;
		if (x >= 640 - 22)
			x = 640 - 22;
		if(y <= 0)
			y=0;
		if(y >= 520 - 22)
			y = 520 - 22;
		
		for(int i = 0; i< game.eb.size(); i++)
			
		{
			EntityB tempEnt = game.eb.get(i);
			if (Physics.Collision(this, tempEnt))
			{
				controles.removeEntity(tempEnt);
				Game.VIDA -= 10;
				game.setInimigo_eliminado(game.getInimigo_eliminado() + 1);
				
			}
		}
		
		
		anim.runAnimation();
		}
	public Rectangle getBounds(){
		return new Rectangle((int)x, (int)y, 32, 32);
	}
		

	
	public void render (Graphics g){
		anim.drawAnimation(g, x, y, 0);
		
	}
	
	public double getX(){
		return x;
		
	}
	public double getY(){
		return y;
	}
	public void setX(double x){
		this.x = x;
	}
	public void setY(double y){
		this.y = y;
	}
	public void setVelX(double velX){
		this.velX = velX;
	}
	public void setVelY(double velY){
		this.velY = velY;
	}

}