import java.awt.image.BufferedImage;

public class Texturas {
	
	public BufferedImage[] player = new BufferedImage[3];
	public BufferedImage[] tiro = new BufferedImage[1];
	public BufferedImage[] inimigo = new BufferedImage[3];
	
    private SpriteSheet ss;
	
	public Texturas(Game game){
		ss = new SpriteSheet(game.getSpriteSheet());
		
		getTexturas();
		
		
	}
	private void getTexturas(){
		player[0] = ss.grabImage(1, 1, 32, 32);
		player[1] = ss.grabImage(1, 2, 32, 32);
		player[2] = ss.grabImage(1, 3, 32, 32);
		
		tiro[0] = ss.grabImage(2, 1, 32, 32);
		
		inimigo[0] = ss.grabImage(3, 1, 32, 32);
		inimigo[1] = ss.grabImage(3, 2, 32, 32);
		inimigo[2] = ss.grabImage(3, 3, 32, 32);
		
	}

}
