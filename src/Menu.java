import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class Menu {
	
	public Rectangle botaoJogar = new Rectangle(Game.WIDHT/2 + 120, 200, 100, 50);
	public Rectangle botaoSair = new Rectangle(Game.WIDHT/2 + 120, 300, 100, 50);
	
	
	public void render (Graphics g){
		
		Graphics2D g2d = (Graphics2D) g;
		Font fnt0 = new Font("times new roman",Font.BOLD,40);
		g.setFont(fnt0);
		g.setColor(Color.WHITE);
		g.drawString("EP2 - Space Game", Game.WIDHT/2, 100);	
		Font fnt1 = new Font("arial", Font.BOLD,30);
		g.setFont(fnt1);
		g2d.draw(botaoJogar);
		g.drawString("Jogar", botaoJogar.x + 10 , botaoJogar.y+35);
		g2d.draw(botaoSair);
		g.drawString("Sair", botaoSair.x + 22 , botaoSair.y+35);
	}

}
